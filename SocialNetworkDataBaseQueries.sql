﻿use master;
go

CREATE DATABASE SocialNetworkDB
ON
(
	NAME = 'SocialNetworkDB',
	FILENAME = 'C:\Projects\FozzyGroupInternship\Practise\SQL\SocialNetworkDB.mdf',
	SIZE = 10MB,
	MAXSIZE = 100MB,
	FILEGROWTH = 10MB
)
LOG ON
(
	NAME = 'LogSocialNetworkDB',
	FILENAME = 'C:\Projects\FozzyGroupInternship\Practise\SQL\LogSocialNetworkDB.ldf',
	SIZE = 5MB,
	MAXSIZE = 50MB,
	FILEGROWTH = 5MB
)
COLLATE Cyrillic_General_CI_AS;
go

use SocialNetworkDB;
go


CREATE TABLE Users
(
	Id int PRIMARY KEY NOT NULL,
	ULogin varchar(40) UNIQUE NOT NULL,
	UPassword varchar(16) NOT NULL,
	UName varchar(20) NOT NULL,
	LName varchar(40) NOT NULL,
	BirthDate date,
	EMail varchar(45) NOT NULL CHECK (EMail LIKE '%_@__%.__%'),
	Phone char(12) CHECK (Phone LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	City varchar(35),
	Adress varchar(70),
	RegistrationDate datetime NOT NULL,
	RelationshipStatus varchar(14) CHECK (RelationshipStatus IN ('InRelationship', 'Single', 'Married', 'CivilMarriage', 'Divorced'))
);
go

CREATE TABLE AudioStore
(
	Id int IDENTITY PRIMARY KEY NOT NULL,
	Link varchar(max) NOT NULL,
	NameAudio varchar(25),
	Author varchar(50),
	Size int,
);
go

CREATE TABLE PlayLists
(
	Id int PRIMARY KEY NOT NULL,
	UserId int NOT NULL 
		FOREIGN KEY REFERENCES Users(Id),
	NamePlayLists varchar(60),
	DatePlayListAdd date NOT NULL
);
go

CREATE TABLE AudioStore_PlayLists
(
	AudioId int UNIQUE NOT NULL
		FOREIGN KEY  REFERENCES AudioStore(Id),
	PlayListId int NOT NULL
		FOREIGN KEY REFERENCES PlayLists(Id),
	DateAudioAdd datetime NOT NULL,
	PRIMARY KEY (AudioId, PlayListId)
);
go

CREATE TABLE VideoStore
(
	Id int IDENTITY PRIMARY KEY NOT NULL,
	Link varchar(max) NOT NULL,
	NameVideo varchar(50),
	Author varchar(50),
	DescriptionAudio varchar(500),
	Size int,
	DateCreateVideo datetime
);
go

CREATE TABLE VideoStore_Users
(
	VideoId int UNIQUE NOT NULL
		FOREIGN KEY REFERENCES VideoStore(Id),
	UserId int NOT NULL
		FOREIGN KEY REFERENCES Users(Id),
	DateVideoAdd datetime NOT NULL,
	PRIMARY KEY (VideoId, UserId)
);
go

CREATE TABLE GameStore
(
	Id int IDENTITY NOT NULL PRIMARY KEY,
	Link varchar(max) NOT NULL,
	NameGame varchar(100) NOT NULL,
	CompanyName varchar(70),
	DescriptionGame varchar(200),
	Size int,
	DateCreationGame date
);
go

CREATE TABLE GameStore_Users
(
	GameId int NOT NULL
		FOREIGN KEY REFERENCES GameStore(Id),
	UserId int NOT NULL
		FOREIGN KEY REFERENCES Users(Id),
	DateGameAdd datetime NOT NULL,
	PRIMARY KEY (GameId, UserId) 
);
go

CREATE TABLE ImageStore
(
	Id int IDENTITY PRIMARY KEY NOT NULL,
	Link varchar(max) NOT NULL,
	NameImage varchar(100),
	Size int
);
go

CREATE TABLE PhotoAlbom
(
	Id int PRIMARY KEY NOT NULL,
	NamePhotoAlbom varchar(100),
	DatePhotoAlbomAdd datetime NOT NULL,
	DescriptionPhotoAlbom varchar(300),
	UserId int NOT NULL
		FOREIGN KEY REFERENCES Users(Id)
);
go

CREATE TABLE ImageStore_PhotoAlbom
(
	ImageId int UNIQUE NOT NULL
		FOREIGN KEY REFERENCES ImageStore(Id),
	PhotoAlbomId int NOT NULL
		FOREIGN KEY REFERENCES PhotoAlbom(Id),
	PRIMARY KEY (ImageId, PhotoAlbomId)
);
go

CREATE TABLE AvatarAlbom
(
	ImageId int UNIQUE NOT NULL
		FOREIGN KEY REFERENCES ImageStore(Id),
	UserId int NOT NULL
		FOREIGN KEY REFERENCES Users(Id),
	PRIMARY KEY (ImageId, UserId)
);
go

CREATE TABLE CompanyType
(
	Id int IDENTITY PRIMARY KEY NOT NULL,
	TypeName varchar(50) NOT NULL
);
go

CREATE TABLE WorkPlace
(
	Id int PRIMARY KEY NOT NULL,
	CompanyName varchar(100) NOT NULL,
	TypeOfCompany int NOT NULL
		FOREIGN KEY REFERENCES CompanyType(Id),
	DescriptionCompany varchar(300),
	DirectorName varchar(35),
	DirectorLastName varchar(50),
	Phone1 char(12) CHECK (Phone1 LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]') NOT NULL,
	Phone2 char(12) CHECK (Phone2 LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	City varchar(90),
	Adress varchar(200),
	EMail varchar(45) NOT NULL CHECK (EMail LIKE '%_@__%.__%'),
	EmploeeNumber int
);
go

CREATE TABLE WorkPlace_Users
(
	WorkPlaceId int NOT NULL
		FOREIGN KEY REFERENCES WorkPlace(Id),
	EmploeeId int NOT NULL
		FOREIGN KEY REFERENCES Users(Id),
	DateStart date NOT NULL,
	DateEnd date NOT NULL DEFAULT ('Works to the present.'),
	Position varchar(45),
	PRIMARY KEY (WorkPlaceId, EmploeeId)
);
go

CREATE TABLE School
(
	Id int PRIMARY KEY NOT NULL,
	SchoolName varchar(100) NOT NULL,
	DirectorName varchar(35),
	DirectorLastName varchar(50),
	PhoneDirector char(12) CHECK (PhoneDirector LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]') NOT NULL,
	PhoneAdministration char(12) CHECK (PhoneAdministration LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]') NOT NULL,
	City varchar(90),
	Adress varchar(200),
	EMail varchar(45) NOT NULL CHECK (EMail LIKE '%_@__%.__%'),
	TeachersNumber int
);
go

CREATE TABLE School_Users
(
	SchoolId int NOT NULL
		FOREIGN KEY REFERENCES School(Id),
	UserId int NOT NULL
		FOREIGN KEY REFERENCES Users(Id),
	DateStart date NOT NULL,
	DateEnd date NOT NULL DEFAULT ('Study to the present.'),
	Position varchar(45),
	PRIMARY KEY (SchoolId, UserId)
);
go

CREATE TABLE Conversations
(
	Id int NOT NULL PRIMARY KEY,
	DateConversation datetime NOT NULL,
	NameConversation varchar(40) NOT NULL
);
go

CREATE TABLE Conversations_Users
(
	ConversationId int NOT NULL 
		FOREIGN KEY REFERENCES Conversations(Id),
	UserId int NOT NULL
		FOREIGN KEY REFERENCES Users(Id),
	PRIMARY KEY (ConversationId, UserId)
);
go

CREATE TABLE UserMessages
(
	Id int NOT NULL PRIMARY KEY,
	UserId int NOT NULL 
		FOREIGN KEY REFERENCES Users(Id),
	TextMessage varchar(max),
	DateMessage datetime NOT NULL,
	ConversationId int NOT NULL
		FOREIGN KEY REFERENCES Conversations(Id)
);
go

INSERT Users
(Id, ULogin, UPassword, UName, LName, BirthDate, EMail, Phone, City, Adress, RegistrationDate, RelationshipStatus)
VALUES
(1, 'SaraGood', '567rty89yu', 'Sara', 'Doltan', '08/15/1970', 'saramara@gmail.com', '(095)8904578', 'Kiev', 'Malinovskogo 67, ap.456', '12/01/2001', 'Single'),
(2, 'NikDik', 'nik1290dik', 'Nik', 'Dikkenson', '09/02/1999', 'nik@gmail.com', '(097)8906745', 'Nikolaev', 'Lenina 4, ap. 56', '12/01/2001 12:30:34.564', NULL),
(3, 'BadMan', '2390ghj5', 'Luck', 'Forinson', '01/06/2000', 'Forick@gmail.com', '(096)8000745', 'Nikolaev', NULL, '12/01/2001 23:30:34.564', NULL),
(4, 'Cat', 'catcatcat56', 'Lusi', 'Dorn', '12/12/1988', 'Lus@gmail.com', '(098)0006745', NULL, NULL, '12/012/2012 15:30:34.564', 'Married'),
(5, 'RedCat', 'qwert', 'Donna', 'Roolz', '12/12/1988', 'rool@gmail.com', '(095)8906111', 'Nikolaev', 'Lenina 12, ap. 512', '01/01/2008 12:30:34.564', NULL),
(6, 'Owl', 'qwerty23', 'Sara', 'Dikkenson', '08/15/1970', 'dikkk@gmail.com', '(066)8556745', 'Harkov', 'Risaka 45, ap. 156', '02/06/2005 08:33:56.564', 'CivilMarriage'),
(7, 'Mouse', 'poiuyt', 'Djonny', 'Black', '12/12/2000', 'black@gmail.com', '(096)4446745', 'Kiev', 'Geroev Dnepra 14, ap. 556', '12/01/2001 12:30:34.564', NULL),
(8, 'CoolGirl', '567890tyu', 'Lilia', 'Right', '12/12/2012', 'cat@gmail.com', '(066)3456745', 'Kiev', 'Geroev Dnepra 14, ap. 556', '12/01/2001 12:30:34.564', 'Single'),
(9, 'Devil', 'qwert12345', 'Tom', 'Donckan', '12/12/2012', 'tom@gmail.com', '(095)8989745', 'Kiev', 'NEbesnoi Sotni 4, ap. 576', '12/01/2001 12:30:34.564', NULL),
(10, 'Duck', 'qwerty', 'Denial', 'Macdonald', '09/09/1988', 'mc@gmail.com', '(097)8906123', 'Kiev', NULL, '12/05/2001 12:30:34.564', NULL);
go


INSERT AudioStore
(Link, NameAudio, Author, Size)
VALUES
('http://music.i.ua/user/1849879/87925/1065208/', 'Allegro','Vivaldi', 45),
('http://rappro.net/play-song/%D0%9E%D0%A7%D0%95%D0%9D%D0%AC%20%D1%81%D0%BC%D0%B5%D1%88%D0%BD%D0%B0%D1%8F%20%D0%BF%D0%B5%D1%81%D0%BD%D1%8F-%D0%9F%D1%80%D0%BE%20%D0%BA%D0%BE%D1%82%D0%B0', 'Cat song', NULL, 78),
('http://playmus.cc/artist/532938/nirvana/', 'Hello Hello', 'Nirvana', 34),
('http://playmus.cc/track/21030862395621031248/fix_me__/', 'Fix Me', NULL, 45),
('http://playmus.cc/track/32102062395632102469/nentori/', 'Nentori', 'Arilena Ara', 67),
('http://music.i.ua/player/3053441/478/6147/', 'Seasons', 'Vivaldi', NULL),
('http://music.i.ua/player/3053441/478/6147/', 'Seasons', 'Vivaldi', NULL),
('http://music.i.ua/player/3053441/478/6147/', 'Seasons', 'Vivaldi', NULL),
('http://music.i.ua/player/3053441/478/6147/', 'Seasons', 'Vivaldi', NULL),
('https://www.google.com.ua/search?q=cat&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjU4byC9J7ZAhUDDOwKHToVAtEQ_AUICigB&biw=1600&bih=794#imgrc=sKYUQsX9cMoQ0M:', 'cat', NULL, NULL);
go


INSERT PlayLists
(Id, UserId, NamePlayLists, DatePlayListAdd)
VALUES
(1, 1, 'Hot', '06/01/2002'),
(2, 1, 'HotSpring', '06/04/2002'),
(3, 1, 'New', '06/28/2002'),
(4, 2, 'Hot', '01/16/2002'),
(5, 2, 'Love', '01/24/2002'),
(6, 3, 'Rep', '02/17/2003'),
(7, 4, 'Classic', '01/06/2013'),
(8, 5, 'Music', '07/16/2009'),
(9, 6, 'MyMusic', '07/09/2006'),
(10, 7, NULL, '08/23/2002');
go

INSERT AudioStore_PlayLists
(AudioId, PlayListId, DateAudioAdd)
VALUES
(1, 1, '12/01/2002 12:30:34.564'),
(2, 1, '12/01/2002 12:30:34.564'),
(3, 2, '06/23/2002 12:30:34.564'),
(4, 2, '06/23/2002 12:30:34.564'),
(5, 3, '06/29/2002 16:35:34.564'),
(10, 3, '06/29/2002 16:45:34.564'), 
(11, 4, '01/23/2002 10:23:56.564'), 
(12, 5, '01/25/2002 12:20:50.564'), 
(15, 6, '02/19/2003 11:11:45.564'), 
(16, 7, '01/07/2013 12:20:50.564');
go

INSERT VideoStore
(Link, NameVideo, Author, DescriptionAudio, Size, DateCreateVideo)
VALUES
('https://www.youtube.com/watch?v=tD0acmX-7Vg', '5 ЛАЙФХАКОВ с ТЕРМОКЛЕЕМ', 'Дженна Марблс', 'Дженна марблс в данном проверяет Лайфхаки с термоклеем.Озвучил - https://vk.com/evstifПервод - https://vk.com/id30868481 Группа Дженны в ВК - https://vk.com/comedystore Русский ИНСТАГРАМ ДЖенны Марблс - https://www.instagram.com/jennamrus/ Магазин одежды "ComedyShop" - https://goo.gl/ixkPAB', 200, '01/21/2018 12:30:34.564'),
('https://www.youtube.com/watch?v=f8sW_tv0WRI', '10 Interesting Facts That Almost Nobody Knows #1', 'Think Fact', 'This video covers ten unique facts that are my attempt to go over facts no one on YouTube has covered yet.', 400, '01/21/2017 12:30:34.564'),
('https://www.youtube.com/watch?v=R4Im9qPW7Kw', '30 Interesting Facts About Life You Didn’t Know!', 'TheWacky', NULL, 200, '01/22/2017 12:35:34.564'),
('https://www.youtube.com/watch?v=RKMtbcw9ZwE', 'INTERESTING Facts', 'Talltanic', NULL, 400, '01/21/2017 12:30:34.564'),
('https://www.youtube.com/watch?v=qMY3gnIHwC0', '10 Interesting Facts', 'DailyTop10s', NULL, 600, '02/21/2017 12:30:34.564'),
('https://www.youtube.com/watch?v=Xyz0NpURbGY', '10 Scary Yet Beautiful Facts', 'The Top Tens', NULL, 800, '02/20/2018 12:30:34.564'), 
('https://www.youtube.com/watch?v=7Vtl2WggqOg', 'SQL for Beginners', 'Rajamanickam Antonimuthu', NULL, 700, '06/20/2017 12:30:34.564'),
('https://www.youtube.com/watch?v=vX4gtQXtAnQ', 'How to Install Adventure Works 2012', 'Peter Fakory', NULL, 100, '07/20/2017 12:30:34.564'), 
('https://www.youtube.com/watch?v=9Pzj7Aj25lw', 'SQL Basics for Beginners', 'Joey Blue', 'A crash course in SQL.  How to write SQL from scratch in 1 hour.', 400, '09/20/2016 16:30:34.564'),
('https://www.youtube.com/watch?v=hOVSKuFTUiI', 'C# Tutorial 16 Threads', 'Derek Banas', 'Get the Code Here : https://goo.gl/Y2Gtbg', 400, '06/28/2016 17:36:34.564');
go

INSERT VideoStore_Users
(VideoId, UserId, DateVideoAdd)
VALUES
(1, 1, '2018-01-22 12:30:34.563'), 
(5, 2, '2017-01-23 12:35:34.563'), 
(6, 3, '2017-06-22 12:35:34.563'), 
(11, 4, '2017-06-21 12:30:34.563'), 
(16, 5, '2017-05-21 12:30:34.563'), 
(17, 6, '2018-02-27 12:30:34.563'), 
(18, 7, '2017-06-21 12:30:34.563'), 
(19, 8, '2017-07-23 12:30:34.563'), 
(20, 9, '2016-09-25 16:30:34.563'), 
(21, 10, '2016-06-29 17:36:34.563');
go

INSERT GameStore
(Link, NameGame, CompanyName, DescriptionGame, Size, DateCreationGame)
VALUES
('http://vseigru.net/igry-io/24678-igra-weckit-io.html', 'Weckit', '8Bit', 'This is a multi-user browser game where your opponents can be many kilometers away from you, but at the same time, you can easily fight with each other.', 57, '12/04/2001'),
('http://vseigru.net/igry-io/24655-igra-evades-io.html', 'Evades', '9Room', 'Evades.io is a multi-user browser game. If in most games you had to compete with other players, then in Evades.io your main task is cooperation.', 45, '03/03/2003'),
('http://vseigru.net/igry-io/24495-igra-fightz-io.html', 'Fightz.io', '7Live', NULL, 34, '05/05/2005');
go

INSERT GameStore_Users
(GameId, UserId, DateGameAdd)
VALUES
(1, 1, '2001-12-12 12:23:23.234'),
(1, 2, '2001-12-12 12:23:23.234'), 
(2, 3, '2003-12-14 16:23:23.234'), 
(2, 1, '2003-12-14 16:23:23.234'), 
(3, 5, '2005-08-05 08:09:34.345'), 
(3, 2, '2005-08-05 08:09:34.345');
go

INSERT ImageStore
(Link, NameImage, Size)
Values
('https://www.google.com.ua/imgres?imgurl=https%3A%2F%2Fwww.vetbabble.com%2Fwp-content%2Fuploads%2F2016%2F11%2Fhiding-cat.jpg&imgrefurl=https%3A%2F%2Fwww.vetbabble.com%2Fcats%2Fquestions-cats%2Fwhy-does-my-cat-still-have-fleas%2F&docid=htQzpdKh2L6-bM&tbnid=sKYUQsX9cMoQ0M%3A&vet=10ahUKEwib5auD9J7ZAhWHr6QKHYCSDeoQMwhvKDQwNA..i&w=1600&h=1200&bih=792&biw=1600&q=cat&ved=0ahUKEwib5auD9J7ZAhWHr6QKHYCSDeoQMwhvKDQwNA&iact=mrc&uact=8', 'Cat in somewhere', 12),
('https://www.google.com.ua/imgres?imgurl=http%3A%2F%2Fweknowyourdreams.com%2Fimages%2Fcat%2Fcat-11.jpg&imgrefurl=http%3A%2F%2Fweknowyourdreams.com%2Fcat.html&docid=bTEnvi_vct8WvM&tbnid=cyRGL1MVWlI9TM%3A&vet=10ahUKEwib5auD9J7ZAhWHr6QKHYCSDeoQMwiNAShSMFI..i&w=1933&h=1034&bih=792&biw=1600&q=cat&ved=0ahUKEwib5auD9J7ZAhWHr6QKHYCSDeoQMwiNAShSMFI&iact=mrc&uact=8', 'Red Cat', 10), 
('https://www.google.com.ua/imgres?imgurl=https%3A%2F%2Fwww.thesun.co.uk%2Fwp-content%2Fuploads%2F2017%2F08%2Fnintchdbpict000345244088.jpg%3Fstrip%3Dall%26w%3D960&imgrefurl=https%3A%2F%2Fwww.thesun.co.uk%2Fliving%2F4228915%2Fcurly-haired-cats%2F&docid=DEqdbKgtBCc8LM&tbnid=shR8jql3Uuo0BM%3A&vet=10ahUKEwib5auD9J7ZAhWHr6QKHYCSDeoQMwieAShjMGM..i&w=960&h=624&bih=792&biw=1600&q=cat&ved=0ahUKEwib5auD9J7ZAhWHr6QKHYCSDeoQMwieAShjMGM&iact=mrc&uact=8', 'Cute cat', 3), 
('https://www.google.com.ua/imgres?imgurl=https%3A%2F%2Fthumbs-prod.si-cdn.com%2Fi3vwJpVXw5BDAXgpUVismkkngj4%3D%2F800x600%2Ffilters%3Ano_upscale()%2Fhttps%3A%2F%2Fpublic-media.smithsonianmag.com%2Ffiler%2F49%2F01%2F49014501-c0be-4c40-a54b-034b46a7b2ad%2Fistock-856455230_1.jpg&imgrefurl=https%3A%2F%2Fwww.smithsonianmag.com%2Fsmart-news%2Fcats-can-be-right-or-left-pawed-180967930%2F&docid=9LNUJL-XEGwObM&tbnid=oYvibNQ3qNs-CM%3A&vet=10ahUKEwjfifuzlKDZAhVF46QKHdfLBhI4ZBAzCDQoLDAs..i&w=800&h=600&bih=792&biw=1600&q=cat&ved=0ahUKEwjfifuzlKDZAhVF46QKHdfLBhI4ZBAzCDQoLDAs&iact=mrc&uact=8', 'Smart cat', 8), 
('https://www.google.com.ua/imgres?imgurl=https%3A%2F%2Fi.ytimg.com%2Fvi%2FSfLV8hD7zX4%2Fmaxresdefault.jpg&imgrefurl=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DSfLV8hD7zX4&docid=JkkgT0Wh9FO29M&tbnid=_vUAT67U1HGFHM%3A&vet=10ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwg2KAAwAA..i&w=1280&h=720&bih=794&biw=1600&q=dog&ved=0ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwg2KAAwAA&iact=mrc&uact=8', 'Sad dog', 6), 
('https://www.google.com.ua/imgres?imgurl=https%3A%2F%2Fwww.cesarsway.com%2Fsites%2Fnewcesarsway%2Ffiles%2Fstyles%2Flarge_article_preview%2Fpublic%2FNatural-Dog-Law-2-To-dogs%252C-energy-is-everything.jpg%3Fitok%3DZ-ujUOUr&imgrefurl=https%3A%2F%2Fwww.cesarsway.com%2Fdog-psychology%2Ffive-laws%2Fenergy%2Fenergy-is-everything&docid=cDk-k2kM0_r1XM&tbnid=GuhEcvKOM9yeyM%3A&vet=10ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwg-KAgwCA..i&w=845&h=450&bih=794&biw=1600&q=dog&ved=0ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwg-KAgwCA&iact=mrc&uact=8', 'Happy dog', 9), 
('https://www.google.com.ua/imgres?imgurl=http%3A%2F%2Fstatic2.businessinsider.com%2Fimage%2F559a92d3eab8ea14317254ad%2Fa-contagious-dog-flu-may-be-on-the-rise-in-the-us.jpg&imgrefurl=http%3A%2F%2Fwww.businessinsider.com%2Fdog-flu-symptoms-how-to-prevent-2018-1&docid=dJgd99yQgCI03M&tbnid=_ZVhdJVwqmei9M%3A&vet=10ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwhyKDEwMQ..i&w=1154&h=865&bih=794&biw=1600&q=dog&ved=0ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwhyKDEwMQ&iact=mrc&uact=8', 'dog', 10), 
('https://www.google.com.ua/imgres?imgurl=http%3A%2F%2Fwww.daytondailynews.com%2Frf%2Fimage_large%2F%2FPub%2Fp8%2FCmgSharedContent%2F2017%2F08%2F18%2FImages%2FGettyImages-479671459_20170818003102.jpg%3Fuuid%3DCtuaRIPOEeeYNh_JE1OHxg&imgrefurl=http%3A%2F%2Fwww.daytondailynews.com%2Fnews%2Fdog-lost-during-windstorm-found-years-later%2F2MENQ4urstoUX8a5B6zucL%2F&docid=y0XeQfCheqKeBM&tbnid=kFBmiUZZeAfJhM%3A&vet=10ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwiGAShEMEQ..i&w=800&h=532&bih=794&biw=1600&q=dog&ved=0ahUKEwiH2_rHq6DZAhUQDewKHd_CB6EQMwiGAShEMEQ&iact=mrc&uact=8', 'Husky', 12), 
('https://www.google.com.ua/imgres?imgurl=https%3A%2F%2Fpbs.twimg.com%2Fprofile_images%2F846146544072101888%2F0sLpdiu1_400x400.jpg&imgrefurl=https%3A%2F%2Ftwitter.com%2Fdog_feelings&docid=f2aF281QpaiTRM&tbnid=1yKVY9-d-eplNM%3A&vet=10ahUKEwi8y73Mq6DZAhUDyaQKHSzBADw4ZBAzCBooFjAW..i&w=400&h=400&bih=794&biw=1600&q=dog&ved=0ahUKEwi8y73Mq6DZAhUDyaQKHSzBADw4ZBAzCBooFjAW&iact=mrc&uact=8', 'Funy dog', 5);
go

INSERT PhotoAlbom
(Id, NamePhotoAlbom, DatePhotoAlbomAdd, DescriptionPhotoAlbom, UserId)
VALUES
(1, 'My', '2001-12-12 12:23:23.234', 'My photo', 7),
(2, 'I am', '2001-07-23 12:23:23.234', 'Summer', 8), 
(3, 'Cat', '2013-12-12 12:23:23.234', 'Lovely cat', 9);
go

INSERT ImageStore_PhotoAlbom
(ImageId, PhotoAlbomId)
VALUES
(1, 1), 
(2, 2), 
(3, 3);
go

INSERT AvatarAlbom
(ImageId, UserId)
VALUES 
(3, 2), 
(4, 3), 
(5, 4), 
(6, 5), 
(7, 6), 
(8, 7), 
(9, 8);
go

INSERT CompanyType
(TypeName)
VALUES
('medicine'), 
('teaching'), 
('media'), 
('services'), 
('IT');
go

INSERT WorkPlace
(Id, CompanyName, TypeOfCompany, DescriptionCompany, DirectorName, DirectorLastName, Phone1, Phone2, City, Adress, EMail, EmploeeNumber)
VALUES
(1, 'Health People', 1, 'consultation and treatment', 'Sam', 'Biltom', '(098)3455667', '(095)1234567', 'Kiev', 'Timoshenka 46', 'HealthPeople@gmail.com', 56), 
(2, 'English school', 2, 'teaching', 'Tom', 'Gir', '(098)2356489', '(099)9999999', 'Kiev', 'Petrova 45', 'EnglishSchool@gmail.com', 12), 
(3, 'Media Plus', 3, 'media', 'Lusi', 'Gacke', '(098)4563434', '(098)4563430', 'Nikolaev', 'Pobedi 12', 'MediaPlus@gmail.com', 50), 
(4, 'Hot Restaurant', 4, 'food services', 'Lara', 'Galisimmo', '(096)4063434', '(098)4003430', 'Harkov', 'Pobedi 16', 'HotRestaurant@gmail.com', 123), 
(5, 'Ciklum', 5, 'software development', 'Denis', 'Lisko', '(095)4443434', '(098)4234430', 'Kiev', 'Garkova 15', 'Ciklum@gmail.com', 230);
go

INSERT WorkPlace_Users
(WorkPlaceId, EmploeeId, DateStart, DateEnd, Position)
VALUES
(1, 1, '09/15/1990', GETDATE(), 'doctor'), 
(3, 2, '2013-09-02', '', 'manager'), 
(2, 4, '2000-12-12', '2009-12-12', 'teacher'), 
(3, 5, '1999-12-12', '2017-12-12', 'actor'), 
(4, 6, '1992-08-15', '', 'cook'), 
(5, 7, '2018-01-01', '', 'c# developer'),   
(5, 10, '2000-09-09', '', 'team lead');
go

INSERT School
(Id, SchoolName, DirectorName, DirectorLastName, PhoneDirector, PhoneAdministration, City, Adress, EMail, TeachersNumber)
VALUES
(1, 'School # 2', 'Dina', 'Lubism', '(098)0000045', '(095)4443535', 'Nikolaev', 'Pobedy 67', 'School2@gmail.com', 189), 
(2, 'School # 22', 'Mark', 'Spencer', '(096)4500045', '(096)4389535', 'Kiev', 'Krakovskaya 12', 'School22@gmail.com', 230);
go


INSERT School_Users
(SchoolId, UserId, DateStart, DateEnd, Position)
VALUES
(1, 3, '2006-01-06', '', '10 forms'), 
(2, 8, '2018-01-01', '', '1 forms'), 
(2, 9, '2018-01-01', '', '1 forms');
go

INSERT Conversations
(Id, DateConversation, NameConversation)
VALUES
(1, '2018-01-14 12:30:34.563', 'NY'),
(2, '2018-01-15 12:30:34.563', 'Cat'),
(3, '2018-01-16 12:30:34.563', 'Dog'),
(4, '2018-01-17 12:30:34.563', 'NY presents'),
(5, '2018-01-11 12:30:34.563', 'Wedding preparation'),
(6, '2018-01-18 12:30:34.563', 'Homework'),
(7, '2018-01-19 12:30:34.563', 'Pets'),
(8, '2018-01-20 12:30:34.563', 'My birthday'),
(9, '2018-01-21 12:30:34.563', 'walk'),
(10, '2018-01-22 12:30:34.563', 'Sport');
go 

INSERT Conversations_Users
(ConversationId, UserId)
VALUES
(1, 1), 
(1, 2), 
(1, 3),
(2, 2), 
(2, 4), 
(3, 5), 
(3, 6), 
(4, 7), 
(4, 8), 
(5, 9), 
(5, 10), 
(6, 7), 
(6, 9), 
(7, 1), 
(7, 2), 
(8, 8), 
(8, 6), 
(9, 5), 
(9, 4), 
(10, 3), 
(10, 10);
go

INSERT UserMessages
(Id, UserId, TextMessage, DateMessage, ConversationId)
VALUES
(1, 1, 'Hello', '2018-01-14 13:30:34.563', 1), 
(2, 2, 'Hello', '2018-01-14 14:30:34.563', 1), 
(3, 3, 'Hello', '2018-01-14 15:30:34.563', 1), 
(4, 2, 'Hi', '2018-01-15 19:30:34.563', 2),
(5, 4, 'Hi', '2018-01-15 20:30:34.563', 2),
(6, 5, 'Good idea', '2018-01-16 13:30:34.563', 3),
(7, 6, 'Good idea', '2018-01-16 14:30:34.563', 3),
(8, 7, 'Look', '2018-01-17 13:30:34.563', 4),
(9, 8, 'Good', '2018-01-17 13:36:34.563', 4),
(10, 9, 'Milk', '2018-01-11 12:35:34.563', 5),
(11, 10, 'Drink', '2018-01-11 12:38:34.563', 5),
(12, 7, 'Hot', '2018-01-18 12:31:34.563', 6),
(13, 8, 'Cool', '2018-01-18 12:32:34.563', 6),
(14, 1, 'Water', '2018-01-19 13:30:34.563', 7),
(15, 2, 'Stone', '2018-01-19 14:30:34.563', 7),
(16, 6, 'Dog', '2018-01-20 13:30:34.563', 8),
(17, 8, 'Cat', '2018-01-20 14:35:34.563', 8),
(18, 4, 'fun', '2018-01-21 12:40:34.563', 9),
(19, 5, 'have fun', '2018-01-21 12:45:34.563', 9),
(20, 3, 'have fun', '2018-01-22 13:30:34.563', 10),
(21, 10, 'have fun', '2018-01-22 14:30:34.563', 10);
go

SELECT * FROM Users;
SELECT * FROM AudioStore;
SELECT * FROM PlayLists;
SELECT * FROM AudioStore_PlayLists;
SELECT * FROM VideoStore;
SELECT * FROM VideoStore_Users;
SELECT * FROM GameStore;
SELECT * FROM GameStore_Users;
SELECT * FROM ImageStore;
SELECT * FROM PhotoAlbom;
SELECT * FROM ImageStore_PhotoAlbom;
SELECT * FROM AvatarAlbom;
SELECT * FROM CompanyType;
SELECT * FROM WorkPlace;
SELECT * FROM WorkPlace_Users;
SELECT * FROM School;
SELECT * FROM School_Users;
SELECT * FROM Conversations;
SELECT * FROM Conversations_Users;
SELECT * FROM UserMessages;
